<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\AutorController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\EscritoController;
use App\Http\Controllers\RentaController;
use App\Http\Controllers\EjemplarController;
use app\Http\Controllers\controllerinicio;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Expr\FuncCall;
use App\Models\Libro;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcom', function(){
    $libros = Libro::paginate(4);
    return view('welcom', compact('libros'));
});
//direncion de la pagina principal de la pagina inicio

Route::resource('libros',  LibroController::class)->middleware('auth');
Route::resource('autor',  AutorController::class)->middleware('auth');
Route::resource('usuarios',  UsuarioController::class)->middleware('auth');
Route::resource('escritores',  EscritoController::class)->middleware('auth');
Route::resource('prestamos',  RentaController::class)->middleware('auth');
Route::resource('ejemplars',  EjemplarController::class)->middleware('auth');

Auth::routes();
Route::group(['middleware'=>'auth'], function(){
    Route::get('/',[LibroController::class, 'index'])->name('home');
});




