<?php

namespace App\Http\Controllers;

use App\Models\Autor;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class AutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Autor = Autor::paginate(4);
        return view('autor.index', compact('Autor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('autor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'nombre'=>'required',
            'ap'=>'required',
            'am'=>'required',
        ];

        $mensaje = [
            'required'=>'El :attribute es requerido',
        ];

        $this->validate($request, $validate, $mensaje);

        $request = request()->except('_token');

        Autor::insert($request);

        return redirect('autor')->with('mensaje', 'Agregado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Autor  $Autor
     * @return \Illuminate\Http\Response
     */
    public function show(Autor $Autor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Autor  $Autor
     * @return \Illuminate\Http\Response
     */
    public function edit(Autor $Autor)
    {
        return view('autor.edit', ['Autor'=>$Autor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Autor  $Autor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valida = [
            'nombre'=>'required',
            'ap'=>'required',
            'am'=>'required',
        ];
        $mensaje = [
            'required'=>'El :attribute es requerido'
        ];
        $this->validate($request, $valida, $mensaje);
        
        $autor = Autor::find($id);
        $autor->nombre = $request->nombre;
        $autor->ap = $request->ap;
        $autor->am = $request->am;
        $autor->save();
        return redirect('autor')->with('mensaje', 'Actualizado Corecctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Autor  $Autor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Autor::destroy($id);
        return redirect('autor')->with('mensaje','Eliminado Correctamente');
    }
}
