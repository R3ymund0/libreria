<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::paginate(5);
        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = [
            'nombre'=> 'required',
            'direccion'=>'required',
            'telefono'=>'Integer'
        ];
        
        $mensaje = [
            'required'=> 'El :attrinute son requeridos',
        ];

        $this->validate($request, $validacion, $mensaje);
        $request = request()->except('_token');
        Usuario::insert($request);
        return redirect('usuarios')->with('mensaje', 'Agregado Correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $Usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $Usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $Usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $Usuario)
    {

        return view('usuarios.edit', ['usuario'=> $Usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $Usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion = [
            'nombre'=> 'required',
            'direccion'=>'required',
            'telefono'=>'Integer'
        ];
        
        $mensaje = [
            'required'=> 'El :attrinute son requeridos',
        ];

        $this->validate($request, $validacion, $mensaje);
        $usuario = Usuario::find($id);
        $usuario->nombre=$request->nombre;
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->save();
        return redirect('usuarios')->with('mensaje', 'Actualizado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $Usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Usuario::destroy($id);
        return redirect('usuarios')->with('mensaje', 'Eliminado Correctamente');
    }
}
