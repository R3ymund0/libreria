<?php

namespace App\Http\Controllers;

use App\Models\Renta;
use Illuminate\Http\Request;

class RentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rentas = Renta::paginate(4);
        return view('prestamos.index', compact('rentas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prestamos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = [
            'fecha_entrega' => 'required',
            'fecha_devolucion' => 'required',
            'ejemplar_id' => 'required',
            'usuario_id' => 'required'
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido'
        ];
        $this->validate($request, $validacion, $mensaje);
        $request = request()->except('_token');
        Renta::insert($request);
        return redirect('prestamos')->with('mensaje', 'Agregado Exisosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Renta  $Renta
     * @return \Illuminate\Http\Response
     */
    public function show(Renta $Renta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Renta  $Renta
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $renta = Renta::find($id);
        return view('prestamos.edit', ['rentas' => $renta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Renta  $Renta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion = [
            'fecha_entrega' => 'required',
            'fecha_devolucion' => 'required',
            'ejemplar_id' => 'required',
            'usuario_id' => 'required'
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido'
        ];
        $this->validate($request, $validacion, $mensaje);
        $renta = Renta::find($id);
        $renta->fecha_entrega = $request->fecha_entrega;
        $renta->fecha_devolucion = $request->fecha_devolucion;
        $renta->ejemplar_id = $request->ejemplar_id;
        $renta->usuario_id = $request->usuario_id;
        $renta->save();
        return redirect('prestamos')->with('mensaje', 'Actualizado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Renta  $Renta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Renta::destroy($id);
        return redirect('prestamos')->with('mensaje','Eliminado correctamente');
    }
}
