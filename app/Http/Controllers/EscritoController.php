<?php

namespace App\Http\Controllers;

use App\Models\Escrito;
use Illuminate\Http\Request;

class EscritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escritor = Escrito::paginate(4);
        return view('escritores.index', compact('escritor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('escritores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = [
            'fecha'=> 'required',
            'autor_id'=> 'required',
            'libro_id'=>'required'
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido',
        ];
        $this->validate($request, $validacion, $mensaje);
        $request = request()->except('_token');
        Escrito::insert($request);
        return redirect('escritores')->with('mensaje', 'Agregado Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Escrito  $Escrito
     * @return \Illuminate\Http\Response
     */
    public function show(Escrito $Escrito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Escrito  $Escrito
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $escrito = Escrito::find($id);
        return view('escritores.edit', ['escrito' => $escrito]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Escrito  $Escrito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion = [
            'fecha'=> 'required',
            'autor_id'=> 'required',
            'libro_id'=>'required'
        ];
        $mensaje = [
            'required' => 'El :attribute es requerido',
        ];
        $this->validate($request, $validacion, $mensaje);

        $escritor = Escrito::find($id);
        $escritor->fecha = $request->fecha;
        $escritor->autor_id = $request->autor_id;
        $escritor->libro_id = $request->libro_id;
        $escritor->save();
        return redirect('escritores')->with('mensaje', 'Actualizado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Escrito  $Escrito
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Escrito::destroy($id);
        return redirect('escritores')->with('mensaje', 'Eliminado Correctamente');
    }
}
