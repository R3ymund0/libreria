<?php

namespace App\Http\Controllers;

use App\Models\Ejemplar;
use Illuminate\Http\Request;

class EjemplarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ejemplares = Ejemplar::paginate(4);
        //$ejemplares = Ejemplar::select('','id')->get();
        return view('ejemplars.index', compact('ejemplares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ejemplars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valicasion = [
            'localizacion' => 'required',
            'libro_id'=>'required'
        ];
        $mensaje = [
            'required'=>'El :attribute es requerido'
        ];
        $this->validate($request, $valicasion, $mensaje);
        $request = request()->except('_token');
        Ejemplar::insert($request);
        return redirect('ejemplars')->with('mensaje', 'Agregado Exitosamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ejemplar  $Ejemplar
     * @return \Illuminate\Http\Response
     */
    public function edit(Ejemplar $Ejemplar)
    {
        return view('ejemplars.edit', ['ejemplar'=>$Ejemplar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ejemplar  $Ejemplar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valicasion = [
            'localizacion' => 'required',
            'libro_id'=>'required'
        ];
        $mensaje = [
            'required'=>'El :attribute es requerido'
        ];
        $this->validate($request, $valicasion, $mensaje);
        $ejemplar = Ejemplar::find($id);
        $ejemplar->localizacion = $request->localizacion;
        $ejemplar->libro_id = $request->libro_id;
        $ejemplar->save();
        return redirect('ejemplars')->with('mensaje', 'Actualizado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ejemplar  $Ejemplar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ejemplar::destroy($id);
        return redirect('ejemplars')->with('mensaje', 'Eliminado Exitosamente');
    }
}
