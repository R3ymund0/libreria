<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = Libro::paginate(4);
        return view('libros.index', compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = [
            'isbn'=> 'required',
            'titulo'=> 'required',
            'editorial'=>'required',
            'npaginas'=>'required'
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido',
        ];
        $this->validate($request, $validacion, $mensaje);
        $request = request()->except('_token');
        Libro::insert($request);
        

        return redirect('libros')->with('mensaje','Agregado con exito');


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Libro  $Libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $Libro)
    {
        /*$libro = Libro::find($Libro->id);*/
        //$this->isbn = $libro->isbn;
        return view('libros.edit', ['libro' => $Libro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Libro  $Libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validacion = [
            'isbn'=> 'required',
            'titulo'=> 'required',
            'editorial'=>'required',
            'npaginas'=>'required'
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido',
        ];
        $this->validate($request, $validacion, $mensaje);

        $Libro = Libro::find($id);
        $Libro->isbn = $request->isbn;
        $Libro->titulo = $request->titulo;
        $Libro->editorial = $request->editorial;
        $Libro->npaginas = $request->npaginas;
        $Libro->save();

        return redirect('libros')->with('mensaje', 'Actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Libro  $Libro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Libro::destroy($id);
        return redirect('libros')->with('mensaje', 'Eliminado con exito');
    }
}
