@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ url('/ejemplars') }}" method="post">
            @csrf
            <h1> Crear Ubicasion </h1>
            <div class="row">
                <div class="col">
                    <label for="localizacion">Localización</label>
                    <input type="text" name="localizacion" id="localizacion" value="{{ old('localizacion') }}"
                        class="form-control @error('localizacion') is-invalid @enderror">
                    @error('localizacion') <div class="invalid-feedback">{{ $message }}</div> @enderror

                </div>
                <div class="col">

                    @php
                        $libros = App\Models\Libro::all();
                    @endphp
                    <label for="libro_id">Ejemplar</label>
                    <select name="libro_id" id="libro_id" class="form-control @error('libro_id') is-invalid @enderror">
                        <option value="">Seleccione un Libro</option>
                        @foreach ($libros as $libro)
                            <option value="{{ $libro->id }}">
                                {{ $libro->titulo }}
                            </option>
                        @endforeach
                    </select>
                    @error('libro_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Guardar">
                    <a class="btn btn-primary" href="{{ url('/ejemplars') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
