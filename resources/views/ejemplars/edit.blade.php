@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/ejemplars/' . $ejemplar->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <h1> Actualizar Ejemplar </h1>

            <div class="row">
                <div class="col">
                    <label for="localizacion">Localizacion</label>
                    <input type="text" name="localizacion" id="localizacion"
                        value="{{ isset($ejemplar->localizacion) ? $ejemplar->localizacion : old('localizacion') }}"
                        class="form-control @error('localizacion') is-invalid @enderror">
                    @error('localizacion') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    @php
                        $libros = App\Models\Libro::all();
                    @endphp
                    <label for="libro_id">Ejemplar</label>
                    <select name="libro_id" id="libro_id" class="form-control @error('libro_id') is-invalid @enderror">
                        <option value="">Seleccione un Libro</option>
                        @foreach ($libros as $libro)
                            <option value="{{ $libro->id }}" @if ($ejemplar->libro_id == $libro->id) selected='selected' @endif>
                                {{ $libro->titulo }}</option>
                            </option>
                        @endforeach
                    </select>
                    @error('libro_id') <div class="invalid-feedback">{{ $message }}</div> @enderror

                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Actualizar">
                    <a class="btn btn-primary" href="{{ url('/ejemplars') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
