@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/ejemplars/create') }}" class="btn btn-success">Crear nuevo</a>

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>LOCALIZACIÓN</th>
                    <th>TITULO</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ejemplares as $ejemplar)
                    <tr>
                        <td>{{ $ejemplar->localizacion }}</td>
                        <td>{{ $ejemplar->libro_id }}</td>
                        <td><a class="btn btn-primary" href="{{ url('/ejemplars/' . $ejemplar->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/ejemplars/' . $ejemplar->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $ejemplares->links() !!}
    </div>
@endsection