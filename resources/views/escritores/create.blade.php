@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ url('/escritores') }}" method="post">
            @csrf
            <h1> Escritor </h1>
            <div class="row">
                <div class="col">
                    <label for="fecha">Fecha</label>
                    <input type="date" name="fecha" id="fecha" value="{{ old('fecha') }}"
                        class="form-control @error('fecha') is-invalid @enderror">
                    @error('fecha') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    @php
                        $autor = App\Models\Autor::all();
                    @endphp
                    <label for="autor_id">autor</label>
                    <select name="autor_id" id="autor_id" class="form-control @error('autor_id') is-invalid @enderror">
                        <option value="">Seleccione un autor</option>
                        @foreach ($autor as $autores)
                            <option value="{{ $autores->id }}">
                                {{ $autores->nombre }}
                            </option>
                        @endforeach
                    </select>
                    @error('autor_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    @php
                        $libro = App\Models\Libro::all();
                    @endphp
                    <label for="libro_id">Usuario</label>
                    <select name="libro_id" id="libro_id" class="form-control @error('libro_id') is-invalid @enderror">
                        <option value="">Seleccione un Usuario</option>
                        @foreach ($libro as $libros)
                            <option value="{{ $libros->id }}">
                                {{ $libros->titulo }}
                            </option>
                        @endforeach
                    </select>
                    @error('libro_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Guardar">
                    <a class="btn btn-primary" href="{{ url('/escritores') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection