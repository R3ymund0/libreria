@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/escritores/create') }}" class="btn btn-success">Crear nuevo</a>

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>FECHA</th>
                    <th>LIBRO</th>
                    <th>AUTOR</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($escritor as $escritores)
                    <tr>
                        <td>{{ $escritores->fecha }}</td>
                        <td>{{ $escritores->libro_id }}</td>
                        <td>{{ $escritores->autor_id }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ url('/escritores/' . $escritores->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/escritores/' . $escritores->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $escritor->links() }}
    </div>
@endsection
