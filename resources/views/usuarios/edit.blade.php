@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/usuarios/' . $usuario->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <h1> Actualizar Usuario </h1>

            <div class="row">
                <div class="col">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" value="{{ isset($usuario->nombre) ? $usuario->nombre : old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror">
                    @error('nombre') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="direccion">Dirección</label>
                    <input type="text" name="direccion" id="direccion" value="{{ isset($usuario->direccion) ? $usuario->direccion : old('direccion') }}" class="form-control @error('direccion') is-invalid @enderror">
                    @error('direccion') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="telefono">Telefono</label>
                    <input type="text" name="telefono" id="telefono" value="{{ isset($usuario->telefono) ? $usuario->telefono : old('telefono') }}" class="form-control @error('telefono') is-invalid @enderror">
                    @error('telefono') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Actualizar">
                    <a class="btn btn-primary" href="{{ url('/usuarios') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
