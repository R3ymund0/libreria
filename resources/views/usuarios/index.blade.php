@extends('layouts.app')

@section('content')
    <div class="container">

        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/usuarios/create') }}" class="btn btn-success">Crear nuevo</a>
        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>NOMBRE</th>
                    <th>DIRECCION</th>
                    <th>TELEFONO</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->nombre }}</td>
                        <td>{{ $usuario->direccion }}</td>
                        <td>{{ $usuario->telefono }}</td>
                        <td><a class="btn btn-primary" href="{{ url('/usuarios/' . $usuario->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/usuarios/' . $usuario->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $usuarios->links() !!}

    </div>
@endsection