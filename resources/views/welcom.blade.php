@extends('layouts.app')

@section('content')
    <div class="container">
    <img src="pic_trulli.jpg" alt="Italian Trulli">
    <img width="200" height="200" src="data:image/png;base64," alt="Italian Trulli">

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>ISBN</th>
                    <th>TITULO</th>
                    <th>EDITORIAL</th>
                    <th>N° PAGINAS</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($libros as $libro)
                    <tr>
                        <td>{{ $libro->isbn }}</td>
                        <td>{{ $libro->titulo }}</td>
                        <td>{{ $libro->editorial }}</td>
                        <td>{{ $libro->npaginas }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection