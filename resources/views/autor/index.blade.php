@extends('layouts.app')

@section('content')
    <div class="container">

        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/autor/create') }}" class="btn btn-success">Crear nuevo</a>
        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>NOMBRE</th>
                    <th>A.PATERNO</th>
                    <th>A.MATERNO</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Autor as $Autors)
                    <tr>
                        <td>{{ $Autors->nombre }}</td>
                        <td>{{ $Autors->ap }}</td>
                        <td>{{ $Autors->am }}</td>
                        <td><a class="btn btn-primary" href="{{ url('/autor/' . $Autors->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/autor/' . $Autors->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $Autor->links() !!}

    </div>
@endsection
