@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/autor/' . $Autor->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <h1> Actualizar Autor </h1>

            <div class="row">
                <div class="col">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" value="{{ isset($Autor->nombre) ? $Autor->nombre : old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror">
                    @error('nombre') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="ap">Paterno</label>
                    <input type="text" name="ap" id="ap" value="{{ isset($Autor->ap) ? $Autor->ap : old('ap') }}" class="form-control @error('ap') is-invalid @enderror">
                    @error('ap') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="am">Materno</label>
                    <input type="text" name="am" id="am" value="{{ isset($Autor->am) ? $Autor->am : old('am') }}" class="form-control @error('am') is-invalid @enderror">
                    @error('am') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Actualizar">
                    <a class="btn btn-primary" href="{{ url('/autor') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
