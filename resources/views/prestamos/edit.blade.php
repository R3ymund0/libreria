@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/prestamos/' . $rentas->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <h1> Actualizar Prestamo </h1>
            <div class="row">
                <div class="col">
                    <label for="fecha_entrega">Fecha Prestamo</label>
                    <input type="date" name="fecha_entrega" id="fecha_entrega"
                        value="{{ isset($rentas->fecha_entrega) ? $rentas->fecha_entrega : old('fecha_entrega') }}"
                        class="form-control @error('fecha_entrega') is-invalid @enderror">
                    @error('fecha_entrega') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="fecha_devolucion">Fecha Devolucion</label>
                    <input type="date" name="fecha_devolucion" id="fecha_devolucion"
                        value="{{ isset($rentas->fecha_devolucion) ? $rentas->fecha_devolucion : old('fecha_devolucion') }}"
                        class="form-control @error('fecha_devolucion') is-invalid @enderror">
                    @error('fecha_devolucion') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col">
                    @php
                        $ejemplars = App\Models\Ejemplar::all();
                    @endphp
                    <label for="ejemplar_id">Ejemplar</label>
                    <select name="ejemplar_id" id="ejemplar_id"
                        class="form-control @error('ejemplar_id') is-invalid @enderror">
                        <option value="">Seleccione un Ejemplar</option>
                        @foreach ($ejemplars as $ejemplar)
                            <option value="{{ $ejemplar->id }}" @if ($ejemplar->id == $rentas->ejemplar_id) selected='selected' @endif>
                                {{ $ejemplar->localizacion }}
                            </option>
                        @endforeach
                    </select>
                    @error('ejemplar_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    @php
                        $usuario = App\Models\Usuario::all();
                    @endphp
                    <label for="usuario_id">Usuario</label>
                    <select name="usuario_id" id="usuario_id"
                        class="form-control @error('usuario_id') is-invalid @enderror">
                        <option value="">Seleccione un Usuario</option>
                        @foreach ($usuario as $usuarios)
                            <option value="{{ $usuarios->id }}" @if ($usuarios->id == $rentas->usuario_id) selected='selected' @endif>
                                {{ $usuarios->nombre }}
                            </option>
                        @endforeach
                    </select>
                    @error('usuario_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Actualizar">
                    <a class="btn btn-primary" href="{{ url('/prestamos') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
