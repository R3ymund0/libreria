@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/prestamos/create') }}" class="btn btn-success">Crear nuevo</a>

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>FECHA PRESTAMO</th>
                    <th>FECHA DECOLUCIÓN</th>
                    <th>USUARIO</th>
                    <th>EJEMPLAR</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rentas as $prestamos)
                    <tr>
                        <td>{{ $prestamos->fecha_entrega }}</td>
                        <td>{{ $prestamos->fecha_devolucion }}</td>
                        <td>{{ $prestamos->usuario_id }}</td>
                        <td>{{ $prestamos->ejemplar_id }}</td>

                        <td>
                            <a class="btn btn-primary" href="{{ url('/prestamos/' . $prestamos->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/prestamos/' . $prestamos->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $rentas->links() }}
    </div>
@endsection
