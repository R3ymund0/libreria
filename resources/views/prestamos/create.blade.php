@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ url('/prestamos') }}" method="post">
            @csrf
            <h1> Prestamo </h1>
            <div class="row">
                <div class="col">
                    <label for="fecha_entrega">Fecha Prestamo</label>
                    <input type="date" name="fecha_entrega" id="fecha_entrega" value="{{ old('fecha_entrega') }}"
                        class="form-control @error('fecha_entrega') is-invalid @enderror">
                    @error('fecha_entrega') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="fecha_devolucion">Fecha Devolución</label>
                    <input type="date" name="fecha_devolucion" id="fecha_devolucion" value="{{ old('fecha_devolucion') }}"
                        class="form-control @error('fecha_devolucion') is-invalid @enderror">
                    @error('fecha_devolucion') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col">
                    @php
                        $ejemplars = App\Models\Ejemplar::all();
                    @endphp
                    <label for="ejemplar_id">Ejemplar</label>
                    <select name="ejemplar_id" id="ejemplar_id" class="form-control @error('ejemplar_id') is-invalid @enderror">
                        <option value="">Seleccione un Ejemplar</option>
                        @foreach ($ejemplars as $ejemplar)
                            <option value="{{ $ejemplar->id }}">
                                {{ $ejemplar->localizacion }}
                            </option>
                        @endforeach
                    </select>
                    @error('ejemplar_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    @php
                        $usuario = App\Models\Usuario::all();
                    @endphp
                    <label for="usuario_id">Usuario</label>
                    <select name="usuario_id" id="usuario_id" class="form-control @error('usuario_id') is-invalid @enderror">
                        <option value="">Seleccione un Usuario</option>
                        @foreach ($usuario as $usuarios)
                            <option value="{{ $usuarios->id }}">
                                {{ $usuarios->nombre }}
                            </option>
                        @endforeach
                    </select>
                    @error('usuario_id') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Guardar">
                    <a class="btn btn-primary" href="{{ url('/prestamos') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
