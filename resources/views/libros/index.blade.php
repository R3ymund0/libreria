@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has('mensaje'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ session::get('mensaje') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <a href="{{ url('/libros/create') }}" class="btn btn-success">Crear nuevo</a>

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>ISBN</th>
                    <th>TITULO</th>
                    <th>EDITORIAL</th>
                    <th>N° PAGINAS</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($libros as $libro)
                    <tr>
                        <td>{{ $libro->isbn }}</td>
                        <td>{{ $libro->titulo }}</td>
                        <td>{{ $libro->editorial }}</td>
                        <td>{{ $libro->npaginas }}</td>
                        <td><a class="btn btn-primary" href="{{ url('/libros/' . $libro->id . '/edit') }}">Editar</a>

                            <form action="{{ url('/libros/' . $libro->id) }}" class="d-inline" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {!! $libros->links() !!}
    </div>
@endsection
