@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/libros/' . $libro->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <h1> Actualizar Libro </h1>

            <div class="row">
                <div class="col">
                    <label for="isbn">ISBN</label>
                    <input type="text" name="isbn" id="isbn"
                        value="{{ isset($libro->isbn) ? $libro->isbn : old('isbn') }}"
                        class="form-control @error('isbn') is-invalid @enderror">
                    @error('isbn') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="titulo">Titulo</label>
                    <input type="text" name="titulo" id="titulo"
                        value="{{ isset($libro->titulo) ? $libro->titulo : old('titulo') }}"
                        class="form-control @error('titulo') is-invalid @enderror">
                    @error('titulo') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="editorial">Editorial</label>
                    <input type="text" name="editorial" id="editorial"
                        value="{{ isset($libro->editorial) ? $libro->editorial : old('editorial') }}"
                        class="form-control @error('editorial') is-invalid @enderror">
                    @error('editorial') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
                <div class="col">
                    <label for="npaginas">Paginas</label>
                    <input type="text" name="npaginas" id="npaginas"
                        value="{{ isset($libro->npaginas) ? $libro->npaginas : old('npaginas') }}"
                        class="form-control @error('npaginas') is-invalid @enderror">
                    @error('npaginas') <div class="invalid-feedback">{{ $message }}</div> @enderror
                </div>
            </div>
            <div class="row">
                <div class="col" style="padding-top: .7cm;">
                    <input class="btn btn-success" type="submit" value="Actualizar">
                    <a class="btn btn-primary" href="{{ url('/libros') }}">Cancelar</a>
                </div>
            </div>

        </form>
    </div>
@endsection
